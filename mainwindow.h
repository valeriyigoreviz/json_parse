#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "jsonprocessor.h"
#include <QString>
#include "report.h"
#include <QMovie>
#include <QTextDocument>
#include <QPrinter>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_pushButtonRefresh_clicked();

    void on_lineEditPage_returnPressed();

    void on_pushButtonPrevPage_clicked();

    void on_pushButtonNextPage_clicked();

    void updateTable();

    void on_pushButtonSelect_clicked();

    void on_pushButtonPrinter_clicked();

    void printDoc(QPrinter *);

private:
    Ui::MainWindow *ui;

    JsonProcessor *jsonProcessor;

    QVector<int> currPageCounter;

    QVector<Report> currReports;
    QVector<Report> reportsToPrint;

    void initActions();

    QMovie *refresh_anim;

    int updatemode;

    int currPageToPrint;

    QTextDocument *document;

    void printerDialog();
};

#endif // MAINWINDOW_H
