#include "timeconverters.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "jsonprocessor.h"
#include "globalvariables.h"
#include <QStringList>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QDateTime>
#include <QTime>
#include <QHeaderView>
#include "dialogselect.h"
#include <QShortcut>
#include <QPrinter>
#include <QPrintDialog>
#include <QDialog>
#include <QPainter>
#include <QSizeF>
#include <QTextBrowser>
#include <QTextCursor>
#include <QFont>
#include <QTextTableCell>
#include <QTextBlockFormat>
#include <QPrintPreviewDialog>

#define BTN_STYLE "background-color: white; border: none;"
#define OTHER_WIDGETS_STYLE "background-color: white;"
#define WINDOW_TITLE "Report views"

static const QStringList TABLE_HEADERS
{
    "id",
    "Дата",
    "Рецепт",
    "Фр. 1",
    "Фр. 2",
    "Фр. 3",
    "Фр. 4",
    "Фр. 5",
    "Фр. 6",
    "Битум",
    "МД",
    "Целюлоза 1",
    "Целюлоза 2",
    "Пиль",
    "Вр. смеш.",
    "Темп.",
    "Темп. битума",
    "Асфальт"
};

enum columns{
    DATE,
    RECIPE_TITLE,
    FRACTION_1,
    FRACTION_2,
    FRACTION_3,
    FRACTION_4,
    FRACTION_5,
    FRACTION_6,
    BITUMEN,
    MP,
    CELLULOSE_1,
    CELLULOSE_2,
    DUST,
    MIXING_TIME,
    TEMPERATURE,
    TEMPERATURE_BITUMEN,
    ASPHALT
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initActions();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonRefresh_clicked()
{
    updatemode = 0;

    QString sort_item = DialogSelect::sort_item;
    QString sort_type = DialogSelect::sort_type;
    int from_date = DialogSelect::from_date;
    int to_date = DialogSelect::to_date;
    QString recipe_title = DialogSelect::recipe_title;

    this->setWindowTitle(QString(WINDOW_TITLE) + " " + millisecindsToDate(from_date) + " - " + millisecindsToDate(to_date));

    refresh_anim->start();
    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

    ui->lineEditPage->setText("1");
    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonSelect->setEnabled(false);
    ui->lineEditPage->setEnabled(false);
    ui->tableWidgetReports->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);
    jsonProcessor->processJson(0, sort_item, sort_type, from_date, to_date, recipe_title);
}

void MainWindow::on_lineEditPage_returnPressed()
{
    updatemode = 0;

    QString sort_item = DialogSelect::sort_item;
    QString sort_type = DialogSelect::sort_type;
    int from_date = DialogSelect::from_date;
    int to_date = DialogSelect::to_date;
    QString recipe_title = DialogSelect::recipe_title;

    refresh_anim->start();

    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonSelect->setEnabled(false);
    ui->lineEditPage->setEnabled(false);
    ui->tableWidgetReports->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);

    int curr_page_index = ui->lineEditPage->text().toInt();
    qDebug() << currPageCounter.length() << " " << curr_page_index;

    if(0 >= curr_page_index)
    {
        curr_page_index = 1;
        ui->lineEditPage->setText("1");
    }

    if(curr_page_index > currPageCounter.length())
    {
        curr_page_index = currPageCounter.length();
        ui->lineEditPage->setText(QString::number(currPageCounter.length()));
    }

    jsonProcessor->processJson(curr_page_index - 1, sort_item, sort_type, from_date, to_date, recipe_title);
}

void MainWindow::on_pushButtonPrevPage_clicked()
{
    updatemode = 0;

    QString sort_item = DialogSelect::sort_item;
    QString sort_type = DialogSelect::sort_type;
    int from_date = DialogSelect::from_date;
    int to_date = DialogSelect::to_date;
    QString recipe_title = DialogSelect::recipe_title;

    refresh_anim->start();

    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonSelect->setEnabled(false);
    ui->lineEditPage->setEnabled(false);
    ui->tableWidgetReports->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);

    int curr_page_index = ui->lineEditPage->text().toInt();
    curr_page_index--;
    ui->lineEditPage->setText(QString::number(curr_page_index));
    qDebug() << currPageCounter.length() << " " << curr_page_index;

    if(0 >= curr_page_index)
    {
        curr_page_index = 1;
        ui->lineEditPage->setText("1");
    }

    if(curr_page_index > currPageCounter.length())
    {
        curr_page_index = currPageCounter.length();
        ui->lineEditPage->setText(QString::number(currPageCounter.length()));
    }

    jsonProcessor->processJson(curr_page_index - 1, sort_item, sort_type, from_date, to_date, recipe_title);
}

void MainWindow::on_pushButtonNextPage_clicked()
{
    updatemode = 0;

    QString sort_item = DialogSelect::sort_item;
    QString sort_type = DialogSelect::sort_type;
    int from_date = DialogSelect::from_date;
    int to_date = DialogSelect::to_date;
    QString recipe_title = DialogSelect::recipe_title;

    refresh_anim->start();

    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonSelect->setEnabled(false);
    ui->lineEditPage->setEnabled(false);
    ui->tableWidgetReports->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);

    int curr_page_index = ui->lineEditPage->text().toInt();
    curr_page_index++;
    ui->lineEditPage->setText(QString::number(curr_page_index));
    qDebug() << currPageCounter.length() << " " << curr_page_index;

    if(0 >= curr_page_index)
    {
        curr_page_index = 1;
        ui->lineEditPage->setText("1");
    }

    if(curr_page_index > currPageCounter.length())
    {
        curr_page_index = currPageCounter.length();
        ui->lineEditPage->setText(QString::number(currPageCounter.length()));
    }

    jsonProcessor->processJson(curr_page_index - 1, sort_item, sort_type, from_date, to_date, recipe_title);
}

void MainWindow::updateTable()
{
    if(updatemode == 0)
    {
        ui->tableWidgetReports->setEnabled(true);
        currPageCounter = GlobalVariables::pages_count;
        currReports = GlobalVariables::reports;

        ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
        ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
        ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
        ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
        ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

        refresh_anim->stop();
        ui->pushButtonRefresh->setIcon(QIcon(":/img/refresh.png"));
        ui->tableWidgetReports->setEnabled(true);
        ui->lineEditPage->setEnabled(true);
        ui->pushButtonNextPage->setEnabled(true);
        ui->pushButtonPrevPage->setEnabled(true);
        ui->pushButtonSelect->setEnabled(true);
        ui->pushButtonPrinter->setEnabled(true);

        ui->tableWidgetReports->clear();
        ui->tableWidgetReports->setRowCount(0);
        ui->tableWidgetReports->setColumnCount(18);
        ui->tableWidgetReports->setShowGrid(true);
        ui->tableWidgetReports->setSelectionMode(QAbstractItemView::SingleSelection);
        ui->tableWidgetReports->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->tableWidgetReports->setHorizontalHeaderLabels(TABLE_HEADERS);
        ui->tableWidgetReports->hideColumn(0);

        for(int i = 0; i < currReports.length(); i++)
        {
            Report report = currReports[i];
            ui->tableWidgetReports->setRowCount(ui->tableWidgetReports->rowCount()+1);
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,1, new QTableWidgetItem(QDateTime::fromTime_t(report.getDate()).time().toString()));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,2, new QTableWidgetItem(report.getRecipeTitle()));
            ui->tableWidgetReports->item(ui->tableWidgetReports->rowCount() - 1,2)->setTextColor(QColor("red"));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,3, new QTableWidgetItem(QString::number(report.getFraction1())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,4, new QTableWidgetItem(QString::number(report.getFraction2())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,5, new QTableWidgetItem(QString::number(report.getFraction3())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,6, new QTableWidgetItem(QString::number(report.getFraction4())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,7, new QTableWidgetItem(QString::number(report.getFraction5())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,8, new QTableWidgetItem(QString::number(report.getFraction6())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,9, new QTableWidgetItem(QString::number(report.getBitumen())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,10, new QTableWidgetItem(QString::number(report.getMp())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,11, new QTableWidgetItem(QString::number(report.getCellulose1())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,12, new QTableWidgetItem(QString::number(report.getCellulose2())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,13, new QTableWidgetItem(QString::number(report.getDust())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,14, new QTableWidgetItem(QString::number(report.getMixingTime())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,15, new QTableWidgetItem(QString::number(report.getTemperature())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,16, new QTableWidgetItem(QString::number(report.getTemperatureBitumen())));
            ui->tableWidgetReports->setItem(ui->tableWidgetReports->rowCount() - 1,17, new QTableWidgetItem(QString::number(report.getAsphalt())));
        }
        for(int i = 0; i < 18; i++)
        {
            ui->tableWidgetReports->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        }
        GlobalVariables::pages_count.clear();
        GlobalVariables::reports.clear();
        qDebug() << "Ready";
    }

    else
    {
        if(DialogSelect::global_stat)
        {
            currReports = GlobalVariables::reports;

            for(int i = 0; i < currReports.length(); i++)
            {
                reportsToPrint.append(currReports[i]);
            }

            printerDialog();
            DialogSelect::global_stat = false;
            GlobalVariables::pages_count.clear();
            GlobalVariables::reports.clear();
        }
        else
        {
            QString sort_item = DialogSelect::sort_item;
            QString sort_type = DialogSelect::sort_type;
            int from_date = DialogSelect::from_date;
            int to_date = DialogSelect::to_date;
            QString recipe_title = DialogSelect::recipe_title;

            currPageCounter = GlobalVariables::pages_count;
            currReports = GlobalVariables::reports;

            for(int i = 0; i < currReports.length(); i++)
            {
                reportsToPrint.append(currReports[i]);
            }

            if(currPageToPrint == currPageCounter.length())
            {
                DialogSelect::global_stat = true;
                jsonProcessor->processJson(currPageToPrint, sort_item, sort_type, from_date, to_date, recipe_title);
                currPageToPrint = 0;
            }
            else
            {
                jsonProcessor->processJson(currPageToPrint, sort_item, sort_type, from_date, to_date, recipe_title);
                currPageToPrint++;
            }
            GlobalVariables::pages_count.clear();
            GlobalVariables::reports.clear();
        }
    }
}

void MainWindow::initActions()
{
    this->setWindowIcon(QIcon(":/img/table.png"));

    updatemode = 0;
    currPageToPrint = 0;

    QShortcut *keyCrtlQ = new QShortcut(this);
    keyCrtlQ->setKey(Qt::CTRL + Qt::Key_Q);
    connect(keyCrtlQ, SIGNAL(activated()), this, SLOT(close()));

    jsonProcessor = new JsonProcessor();
    refresh_anim = new QMovie(this);
    refresh_anim->setFileName(":/img/loading.gif");
    connect(refresh_anim, &QMovie::frameChanged, [=]{
        ui->pushButtonRefresh->setIcon(refresh_anim->currentPixmap());});

    connect(jsonProcessor, SIGNAL(finished()), this, SLOT(updateTable()));

    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);

    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);
    ui->widgetActionPanel->setStyleSheet(OTHER_WIDGETS_STYLE);
    ui->widgetEmptyPanel->setStyleSheet(OTHER_WIDGETS_STYLE);

    this->setStyleSheet(OTHER_WIDGETS_STYLE);
}

void MainWindow::printerDialog()
{
    QPrinter printer;
    document=new QTextDocument(this);
    QTextCursor cursor(document);
    cursor.movePosition(QTextCursor::Start);
    QTextTableFormat tableFormat;
    tableFormat.setAlignment(Qt::AlignCenter);
    tableFormat.setBorder(1);
    tableFormat.setCellSpacing(0);
    tableFormat.setWidth(QTextLength(QTextLength::PercentageLength, 100));

    QTextBlockFormat blockFrm;
    blockFrm.setAlignment(Qt::AlignCenter);
    QString reportHeaderTile = "ОТЧЁТ О РАБОТЕ АСФАЛЬТОСМЕСИТЕЛЬНОЙ УСТАНОВКИ\n";
    QString reportHeaderDateRange = QString("Отчёт за период %1 - %2")
            .arg(millisecondsToDateAndTime(DialogSelect::from_date))
            .arg(millisecondsToDateAndTime(DialogSelect::to_date));

    cursor.insertBlock(blockFrm);
    cursor.insertText(reportHeaderTile);

    blockFrm.setAlignment(Qt::AlignLeft);
    QTextCharFormat textCharFromat;
    QFont headerDateFontTmp;
    headerDateFontTmp.setPointSize(7);
    const QFont headerDateFont = headerDateFontTmp;
    textCharFromat.setFont(headerDateFont);
    cursor.insertBlock(blockFrm, textCharFromat);
    cursor.insertText(reportHeaderDateRange);

    cursor.movePosition(QTextCursor::End);

    QTextTable *table = cursor.insertTable(reportsToPrint.length() + 1, 17, tableFormat);

    QTextTableCellFormat cellFormatHeaders;
    QFont tmpHeadersFont;
    tmpHeadersFont.setBold(true);
    tmpHeadersFont.setPointSize(5);
    const QFont fontHeaders = tmpHeadersFont;
    cellFormatHeaders.setFont(fontHeaders);
    cellFormatHeaders.setBackground(QBrush(QColor("lightGray")));

    QTextBlockFormat rightAlighment;
    rightAlighment.setAlignment(Qt::AlignRight);

    for(int i = 1; i < 18; i++)
    {
        QTextTableCell cell = table->cellAt(0, i - 1);
        cell.setFormat(cellFormatHeaders);
        QTextCursor cellCursor = cell.firstCursorPosition();
        cellCursor.setBlockFormat(rightAlighment);
        cellCursor.insertText(TABLE_HEADERS[i] + " ");
    }

    QTextTableCellFormat cellFormatTableCells;
    QFont tmpCellsFont;
    tmpCellsFont.setPointSize(5);
    const QFont cellsFont = tmpCellsFont;
    cellFormatTableCells.setFont(cellsFont);

    for(int i = 0; i < reportsToPrint.length(); i++)
    {
        for(int column = 0; column < TABLE_HEADERS.length() - 1; column++)
        {
            QTextTableCell cell = table->cellAt(i + 1, column);
            cell.setFormat(cellFormatTableCells);
            QTextCursor cellCursor = cell.firstCursorPosition();
            cellCursor.setBlockFormat(rightAlighment);

            QString textCursor = "";
            Report report = reportsToPrint[i];
            switch (column) {
                case DATE:
                    textCursor = millisecondsToDateAndTime(report.getDate());
                    break;
                case RECIPE_TITLE:
                    textCursor = report.getRecipeTitle();
                    break;
                case FRACTION_1:
                    textCursor = QString::number(report.getFraction1());
                    break;
                case FRACTION_2:
                    textCursor = QString::number(report.getFraction2());
                    break;
                case FRACTION_3:
                    textCursor = QString::number(report.getFraction3());
                    break;
                case FRACTION_4:
                    textCursor = QString::number(report.getFraction4());
                    break;
                case FRACTION_5:
                    textCursor = QString::number(report.getFraction5());
                    break;
                case FRACTION_6:
                    textCursor = QString::number(report.getFraction6());
                    break;
                case BITUMEN:
                    textCursor = QString::number(report.getBitumen());
                    break;
                case MP:
                    textCursor = QString::number(report.getMp());
                    break;
                case CELLULOSE_1:
                    textCursor = QString::number(report.getCellulose1());
                    break;
                case CELLULOSE_2:
                    textCursor = QString::number(report.getCellulose2());
                    break;
                case DUST:
                    textCursor = QString::number(report.getDust());
                    break;
                case MIXING_TIME:
                    textCursor = QString::number(report.getMixingTime());
                    break;
                case TEMPERATURE:
                    textCursor = QString::number(report.getTemperature());
                    break;
                case TEMPERATURE_BITUMEN:
                    textCursor = QString::number(report.getTemperatureBitumen());
                    break;
                case ASPHALT:
                    textCursor = QString::number(report.getAsphalt());
            }
            cellCursor.insertText(textCursor + " ");
        }
    }

    printer.setOutputFormat(QPrinter::NativeFormat);
    QPrintPreviewDialog printPreviewDialog(&printer, this);
    printPreviewDialog.setWindowFlags ( Qt::Window );
    QSizeF paperSize;
    paperSize.setWidth(printer.width());
    paperSize.setHeight(printer.height());
    document->setPageSize(paperSize);
    connect(&printPreviewDialog, SIGNAL(paintRequested(QPrinter *)), SLOT(printDoc(QPrinter *)));
    printPreviewDialog.exec();
    reportsToPrint.clear();
}

void MainWindow::on_pushButtonSelect_clicked()
{
    DialogSelect selectWindow;
    selectWindow.setModal(true);
    selectWindow.exec();
}

void MainWindow::on_pushButtonPrinter_clicked()
{
    updatemode = 1;
    currPageToPrint = 0;
    QString sort_item = DialogSelect::sort_item;
    QString sort_type = DialogSelect::sort_type;
    int from_date = DialogSelect::from_date;
    int to_date = DialogSelect::to_date;
    QString recipe_title = DialogSelect::recipe_title;

    ui->pushButtonRefresh->setStyleSheet(BTN_STYLE);
    ui->pushButtonNextPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrevPage->setStyleSheet(BTN_STYLE);
    ui->pushButtonSelect->setStyleSheet(BTN_STYLE);
    ui->pushButtonPrinter->setStyleSheet(BTN_STYLE);

    ui->pushButtonRefresh->setEnabled(false);
    ui->pushButtonNextPage->setEnabled(false);
    ui->pushButtonPrevPage->setEnabled(false);
    ui->pushButtonSelect->setEnabled(false);
    ui->lineEditPage->setEnabled(false);
    ui->tableWidgetReports->setEnabled(false);
    ui->pushButtonPrinter->setEnabled(false);
    jsonProcessor->processJson(currPageToPrint, sort_item, sort_type, from_date, to_date, recipe_title);
    currPageToPrint = 1;
}

void MainWindow::printDoc(QPrinter *printer)
{
    ui->pushButtonRefresh->setEnabled(true);
    ui->pushButtonNextPage->setEnabled(true);
    ui->pushButtonPrevPage->setEnabled(true);
    ui->pushButtonSelect->setEnabled(true);
    ui->lineEditPage->setEnabled(true);
    ui->tableWidgetReports->setEnabled(true);
    ui->pushButtonPrinter->setEnabled(true);
    document->print(printer);
}
