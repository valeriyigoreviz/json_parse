#include "dialogdateselect.h"
#include "ui_dialogdateselect.h"
#include "dialogselect.h"
#include <QShortcut>
#include <QTranslator>


DialogDateSelect::DialogDateSelect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDateSelect)
{
    ui->setupUi(this);
    QShortcut *keyCrtlQ = new QShortcut(this);
    keyCrtlQ->setKey(Qt::CTRL + Qt::Key_Q);
    connect(keyCrtlQ, SIGNAL(activated()), this, SLOT(close()));
    ui->labelTime->setText(QObject::tr("Time"));
}

DialogDateSelect::~DialogDateSelect()
{
    delete ui;
}

void DialogDateSelect::on_buttonBox_accepted()
{
    int datestamp = QDateTime(ui->calendarWidget->selectedDate(), ui->timeEdit->time()).toTime_t();
    if(DialogSelect::is_start_date_button_enable)
    {
        chosen_date_to = datestamp;
    }
    else
    {
        chosen_date_from = datestamp;
    }
    emit finished();
}
