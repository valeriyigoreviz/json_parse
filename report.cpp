#include "report.h"

Report::Report()
{
    id = 0;
    date = 0;
    fraction1 = 0;
    fraction2 = 0;
    fraction3 = 0;
    fraction4 = 0;
    fraction5 = 0;
    fraction6 = 0;
    dust = 0;
    mixingTime = 0;
    temperature = 0;
    temperatureBitumen = 0;
    asphalt = 0;

    bitumen = 0;
    mp = 0;
    cellulose1 = 0;
    cellulose2 = 0;

    recipeTitle = "none";
}

Report::~Report()
{

}

//Set

void Report::setId(int _id)
{
    id = _id;
}

void Report::setDate(int _date)
{
    date = _date;
}

void Report::setFraction1(int _fraction1)
{
    fraction1 = _fraction1;
}

void Report::setFraction2(int _fraction2)
{
    fraction2 = _fraction2;
}

void Report::setFraction3(int _fraction3)
{
    fraction3 = _fraction3;
}

void Report::setFraction4(int _fraction4)
{
    fraction4 = _fraction4;
}

void Report::setFraction5(int _fraction5)
{
    fraction5 = _fraction5;
}

void Report::setFraction6(int _fraction6)
{
    fraction6 = _fraction6;
}

void Report::setDust(int _dust)
{
    dust = _dust;
}

void Report::setMixingTime(int _mixingTime)
{
    mixingTime = _mixingTime;
}

void Report::setTemperature(int _temperature)
{
    temperature = _temperature;
}

void Report::setTemperatureBitumen(int _temperatureBitumen)
{
    temperatureBitumen = _temperatureBitumen;
}

void Report::setAsphalt(int _asphalt)
{
    asphalt = _asphalt;
}

void Report::setBitumen(double _bitumen)
{
    bitumen = _bitumen;
}

void Report::setMp(double _mp)
{
    mp = _mp;
}

void Report::setCellulose1(double _cellulose1)
{
    cellulose1 = _cellulose1;
}

void Report::setCellulose2(double _cellulose2)
{
    cellulose2 = _cellulose2;
}

void Report::setRecipeTitle(QString _recipeTitle)
{
    recipeTitle = _recipeTitle;
}

//End Set

//Get
int Report::getId()
{
    return id;
}

int Report::getDate()
{
    return date;
}

int Report::getFraction1()
{
    return fraction1;
}

int Report::getFraction2()
{
    return fraction2;
}

int Report::getFraction3()
{
    return fraction3;
}

int Report::getFraction4()
{
    return fraction4;
}

int Report::getFraction5()
{
    return fraction5;
}

int Report::getFraction6()
{
    return fraction6;
}

int Report::getDust()
{
    return dust;
}

int Report::getMixingTime()
{
    return mixingTime;
}

int Report::getTemperature()
{
    return temperature;
}

int Report::getTemperatureBitumen()
{
    return temperatureBitumen;
}

int Report::getAsphalt()
{
    return asphalt;
}

double Report::getBitumen()
{
    return bitumen;
}

double Report::getMp()
{
    return mp;
}

double Report::getCellulose1()
{
    return cellulose1;
}

double Report::getCellulose2()
{
    return cellulose2;
}

QString Report::getRecipeTitle()
{
    return recipeTitle;
}

//End Get
