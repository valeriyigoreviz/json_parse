#ifndef REPORT_H
#define REPORT_H

#include <QString>


class Report
{
private:
    int id,
        date,
        fraction1,
        fraction2,
        fraction3,
        fraction4,
        fraction5,
        fraction6,
        dust,
        mixingTime,
        temperature,
        temperatureBitumen,
        asphalt;

    double bitumen,
           mp,
           cellulose1,
           cellulose2;

    QString recipeTitle;
public:
    Report();
    ~Report();

    //Set
    void setId(int _id);
    void setDate(int _date);
    void setFraction1(int _fraction1);
    void setFraction2(int _fraction2);
    void setFraction3(int _fraction3);
    void setFraction4(int _fraction4);
    void setFraction5(int _fraction5);
    void setFraction6(int _fraction6);
    void setDust(int _dust);
    void setMixingTime(int _mixingTime);
    void setTemperature(int _temperature);
    void setTemperatureBitumen(int _temperatureBitumen);
    void setAsphalt(int _asphalt);

    void setBitumen(double _bitumen);
    void setMp(double _mp);
    void setCellulose1(double _cellulose1);
    void setCellulose2(double _cellulose2);

    void setRecipeTitle(QString _recipeTitle);

    //Get
    int getId();
    int getDate();
    int getFraction1();
    int getFraction2();
    int getFraction3();
    int getFraction4();
    int getFraction5();
    int getFraction6();
    int getDust();
    int getMixingTime();
    int getTemperature();
    int getTemperatureBitumen();
    int getAsphalt();

    double getBitumen();
    double getMp();
    double getCellulose1();
    double getCellulose2();

    QString getRecipeTitle();
};

#endif // REPORT_H
