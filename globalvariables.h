#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H

#include <QString>
#include <QVector>
#include "report.h"

class GlobalVariables
{
public:
    GlobalVariables();

    static QVector<Report> reports;
    static QVector<int> pages_count;
};

#endif // GLOBALVARIABLES_H
