#ifndef JSONPROCESSOR_H
#define JSONPROCESSOR_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QString>
#include <QVector>

class JsonProcessor: public QObject
{
    Q_OBJECT
public:
    JsonProcessor();

    void processJson(int curr_page,
                     QString sort_item,
                     QString sort_type,
                     int from_date,
                     int to_date,
                     QString recipe_title);

private:
    QNetworkAccessManager *networkManager;

    void parseJsonValueToArray(QJsonValue json_array);

private slots:
    void onResult(QNetworkReply *reply);

signals:
    void finished();
};

#endif // JSONPROCESSOR_H
