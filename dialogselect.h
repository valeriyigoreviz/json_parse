#ifndef DIALOGSELECT_H
#define DIALOGSELECT_H

#include <QDialog>
#include <QDateTime>
#include <QString>
#include <QObject>
#include "dialogdateselect.h"
#include <QNetworkAccessManager>

namespace Ui {
class DialogSelect;
}

class DialogSelect : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSelect(QWidget *parent = nullptr);
    ~DialogSelect();

    static QString sort_type;
    static QString sort_item;
    static int from_date;
    static int to_date;
    static bool global_stat;
    static QString recipe_title;

    static int is_start_date_button_enable;

private slots:
    void on_pushButtonSelectDateFrom_clicked();

    void on_pushButtonSelectDateTo_clicked();

    void update_date_labels();

    void on_buttonBox_accepted();

    void onResult(QNetworkReply *reply);

private:
    Ui::DialogSelect *ui;

    void initActions();

    DialogDateSelect dateSelectWindow;

    QNetworkAccessManager *networkManager;
};

#endif // DIALOGSELECT_H
