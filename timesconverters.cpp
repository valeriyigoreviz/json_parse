#include <QString>
#include <QDateTime>
#include "dialogselect.h"
#include "timeconverters.h"

#define DATE_AND_TIME_PATTERN "d/MM/yy HH:mm"
#define DATE_PATTERN "d MMMM yyyy"

QString millisecondsToDateAndTime(long mills){
   return QDateTime::fromTime_t(mills).toString(DATE_AND_TIME_PATTERN);
}

QString millisecindsToDate(long mills){
    return QDateTime::fromTime_t(mills).toString(DATE_PATTERN);
}


