#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QJsonDocument>
#include <QDebug>
#include <QJsonObject>
#include <QDir>
#include <QIODevice>
#include <QJsonArray>
#include "globalvariables.h"
#include "report.h"
#include "dialogselect.h"

QVector<Report> GlobalVariables::reports;
QVector<int> GlobalVariables::pages_count;

QString DialogSelect::sort_type;
QString DialogSelect::sort_item;
int DialogSelect::from_date;
int DialogSelect::to_date;
int DialogSelect::is_start_date_button_enable;
int DialogDateSelect::chosen_date_from;
int DialogDateSelect::chosen_date_to;
bool DialogSelect::global_stat;
QString DialogSelect::recipe_title;

void initActions()
{
    QFile selectParams(QDir::currentPath() + "/SelectParams.json");
    selectParams.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream selectParamsTextStream(&selectParams);
    QString selectParamsText = selectParamsTextStream.readAll();
    qDebug() << selectParamsText;
    QJsonDocument selectParamsJson = QJsonDocument::fromJson(selectParamsText.toUtf8());
    QJsonObject selectParamsJsonValues = selectParamsJson.object();

    DialogSelect::sort_type = selectParamsJsonValues.value("sort_type").toString();
    DialogSelect::sort_item = selectParamsJsonValues.value("sort_item").toString();
    DialogSelect::from_date = selectParamsJsonValues.value("from_date").toInt();
    DialogSelect::to_date = selectParamsJsonValues.value("to_date").toInt();
    DialogSelect::is_start_date_button_enable = 0;
    DialogSelect::global_stat = selectParamsJsonValues.value("global_stat").toBool();
    DialogSelect::recipe_title = selectParamsJsonValues.value("recipe_title").toString();
    DialogDateSelect::chosen_date_from = selectParamsJsonValues.value("from_date").toInt();
    DialogDateSelect::chosen_date_to = selectParamsJsonValues.value("to_date").toInt();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    initActions();
    return a.exec();
}
