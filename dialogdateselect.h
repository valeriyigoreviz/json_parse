#ifndef DIALOGDATESELECT_H
#define DIALOGDATESELECT_H

#include <QDialog>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QObject>

namespace Ui {
class DialogDateSelect;
}

class DialogDateSelect : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDateSelect(QWidget *parent = nullptr);
    ~DialogDateSelect();

    static int chosen_date_from;
    static int chosen_date_to;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DialogDateSelect *ui;

signals:
    void finished();
};

#endif // DIALOGDATESELECT_H
