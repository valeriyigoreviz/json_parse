#include "dialogselect.h"
#include "ui_dialogselect.h"
#include <QShortcut>
#include <QTranslator>
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QDebug>
#include <QJsonObject>
#include <QDir>
#include <QIODevice>
#include <QJsonArray>
#include <QMessageBox>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
#include "timeconverters.h"

#define LB_DATE_FROM    "Дата от: "
#define LB_DATE_TO      "Дата до: "

#define RECIEPE_TITLES_URL "http://127.0.0.1:8080/recipes"

static const QStringList SORT_ITEMS
{
    "id",
    "date",
    "bitumen"
};

static const QStringList SORT_ITEMS_NAME
{
    QObject::tr("id"),
    QObject::tr("Date"),
    QObject::tr("Bitumen")
};

static const QStringList SORT_TYPES
{
    "asc",
    "desc"
};

static const QStringList SORT_TYPES_NAME
{
    QObject::tr("Ascend"),
    QObject::tr("Descend")
};

DialogSelect::DialogSelect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSelect)
{
    ui->setupUi(this);
    initActions();
    QObject::connect(&dateSelectWindow, SIGNAL(finished()), this, SLOT(update_date_labels()));
}

DialogSelect::~DialogSelect()
{
    delete ui;
}

void DialogSelect::initActions()
{
    QShortcut *keyCrtlQ = new QShortcut(this);
    keyCrtlQ->setKey(Qt::CTRL + Qt::Key_Q);
    connect(keyCrtlQ, SIGNAL(activated()), this, SLOT(close()));

    ui->pushButtonSelectDateTo->setEnabled(false);
    ui->pushButtonSelectDateFrom->setEnabled(false);
    ui->checkBoxGeneralStatistic->setEnabled(false);
    ui->buttonBox->setEnabled(false);
    ui->comboBoxSortItem->setEnabled(false);
    ui->comboBoxSortType->setEnabled(false);
    ui->comboBoxReciepeTittles->setEnabled(false);

    networkManager = new QNetworkAccessManager();
    connect(networkManager, &QNetworkAccessManager::finished, this, &DialogSelect::onResult);

    ui->labelSort->setText(QObject::tr("Sort by"));
    ui->labelTimeTo->setText(QObject::tr("Date to"));
    ui->labelTimeFrom->setText(QObject::tr("Date from"));
    ui->checkBoxGeneralStatistic->setText(QObject::tr("Global statistic"));
    ui->labelRecipe->setText(QObject::tr("Recipes"));

    ui->checkBoxGeneralStatistic->setCheckState(Qt::CheckState(global_stat));

    ui->comboBoxSortItem->addItems(SORT_ITEMS_NAME);
    ui->comboBoxSortItem->setCurrentIndex(SORT_ITEMS.indexOf(sort_item));

    ui->comboBoxSortType->addItems(SORT_TYPES_NAME);
    ui->comboBoxSortType->setCurrentIndex(SORT_TYPES.indexOf(sort_type));

    ui->pushButtonSelectDateFrom->setText(millisecondsToDateAndTime(from_date));
    ui->pushButtonSelectDateTo->setText(millisecondsToDateAndTime(to_date));
    networkManager->get(QNetworkRequest(QUrl(RECIEPE_TITLES_URL)));
}

void DialogSelect::on_pushButtonSelectDateFrom_clicked()
{
    is_start_date_button_enable = 0;
    dateSelectWindow.setModal(true);
    dateSelectWindow.exec();
}

void DialogSelect::on_pushButtonSelectDateTo_clicked()
{
    is_start_date_button_enable = 1;
    dateSelectWindow.setModal(true);
    dateSelectWindow.exec();
}

void DialogSelect::update_date_labels()
{
    ui->pushButtonSelectDateFrom->setText(millisecindsToDate(DialogDateSelect::chosen_date_from));
    ui->pushButtonSelectDateTo->setText(millisecindsToDate(DialogDateSelect::chosen_date_to));
}

void DialogSelect::on_buttonBox_accepted()
{
    sort_type = SORT_TYPES[ui->comboBoxSortType->currentIndex()];
    sort_item = SORT_ITEMS[ui->comboBoxSortItem->currentIndex()];
    recipe_title = ui->comboBoxReciepeTittles->currentText();
    from_date = DialogDateSelect::chosen_date_from;
    to_date = DialogDateSelect::chosen_date_to;
    global_stat = ui->checkBoxGeneralStatistic->checkState();

    QJsonObject recordSelectParams;
    recordSelectParams.insert("sort_type", QJsonValue::fromVariant(sort_type));
    recordSelectParams.insert("sort_item", QJsonValue::fromVariant(sort_item));
    recordSelectParams.insert("from_date", QJsonValue::fromVariant(from_date));
    recordSelectParams.insert("to_date", QJsonValue::fromVariant(to_date));
    recordSelectParams.insert("global_stat", QJsonValue::fromVariant(global_stat));

    QJsonDocument selectParamsDocs(recordSelectParams);
    QFile selectParamsFile(QDir::currentPath() + "/SelectParams.json");
    if(selectParamsFile.open(QIODevice::WriteOnly))
    {
        QTextStream stream( &selectParamsFile );
        stream << selectParamsDocs.toJson() << endl;
    }
    else
    {
         QMessageBox::information(this, "Warning!", "Settings not save");
    }
    selectParamsFile.close();
}

void DialogSelect::onResult(QNetworkReply *reply)
{
    if(!reply->error())
    {
        QJsonDocument document;
        QJsonObject root;
        QJsonValue jsonValue;
        document = QJsonDocument::fromJson("{\"content\" : " + reply->readAll() + "}");
        root = document.object();
        jsonValue = root.value("content");
        if(jsonValue.isArray())
        {
            ui->comboBoxReciepeTittles->addItem("all");
            QJsonArray ja = jsonValue.toArray();
            for(int i = 0; i < ja.count(); i++)
            {
                ui->comboBoxReciepeTittles->addItem(ja.at(i).toString());
            }
            ui->comboBoxReciepeTittles->setCurrentText(recipe_title);
        }
    }
    else
    {
        qDebug() << "Reply error";
        ui->comboBoxReciepeTittles->addItem("ERROR");
    }

    ui->pushButtonSelectDateTo->setEnabled(true);
    ui->pushButtonSelectDateFrom->setEnabled(true);
    ui->checkBoxGeneralStatistic->setEnabled(true);
    ui->buttonBox->setEnabled(true);
    ui->comboBoxSortItem->setEnabled(true);
    ui->comboBoxSortType->setEnabled(true);
    ui->comboBoxReciepeTittles->setEnabled(true);
}
