#include "jsonprocessor.h"
#include "globalvariables.h"
#include "dialogselect.h"
#include "report.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QUrl>
#include <QDebug>

#define SERV_ADDR                   "http://127.0.0.1:8080"
//http://127.0.0.1:5000
//http://172.16.24.242:8080

#define REQUEST_PARAMS "startDate=%1&endDate=%2&page=%3&sort=%4&%4.dir=%5"
#define REQUEST_PARAMS_TOTAL_REPORTS "startDate=%1&endDate=%2"

#define REPORT_ID                   "id"
#define REPORT_DATE                 "date"
#define REPORT_RECIPETITLE          "recipeTitle"
#define REPORT_FRACTION1            "fraction1"
#define REPORT_FRACTION2            "fraction2"
#define REPORT_FRACTION3            "fraction3"
#define REPORT_FRACTION4            "fraction4"
#define REPORT_FRACTION5            "fraction5"
#define REPORT_FRACTION6            "fraction6"
#define REPORT_BITUMEN              "bitumen"
#define REPORT_MP                   "mp"
#define REPORT_CELLULOSE1           "cellulose1"
#define REPORT_CELLULOSE2           "cellulose2"
#define REPORT_DUST                 "dust"
#define REPORT_MIXINGTIME           "mixingTime"
#define REPORT_TEMPERATURE          "temperature"
#define REPORT_TEMPERATUREBITUMEN   "temperatureBitumen"
#define REPORT_ASPHALT              "asphalt"

JsonProcessor::JsonProcessor()
{
    networkManager = new QNetworkAccessManager();
    connect(networkManager, &QNetworkAccessManager::finished, this, &JsonProcessor::onResult);
}

void JsonProcessor::processJson(int curr_page,
                                QString sort_item,
                                QString sort_type,
                                int from_date,
                                int to_date,
                                QString recipe_title)
{
    QString req_params;
    if(DialogSelect::global_stat == false)
    {
        if(recipe_title == "all")
        {
            req_params = "/reports?" +  QString(REQUEST_PARAMS)
                    .arg(from_date)
                    .arg(to_date)
                    .arg(QString::number(curr_page))
                    .arg(sort_item)
                    .arg(sort_type);
        }

        else
        {
            req_params = "/reports?" +  QString(QString(REQUEST_PARAMS) + "&recipeTitle=%6")
                    .arg(from_date)
                    .arg(to_date)
                    .arg(QString::number(curr_page))
                    .arg(sort_item)
                    .arg(sort_type)
                    .arg(recipe_title);
        }
    }

    else
    {
        if(recipe_title == "all")
        {
            req_params = "/totalReports?" +  QString(QString(REQUEST_PARAMS) + "&recipeTitle=%6")
                    .arg(from_date)
                    .arg(to_date);
        }
        else
        {

        }
        req_params = "/totalReports?" +  QString(QString(REQUEST_PARAMS) + "&recipeTitle=%6")
                .arg(from_date)
                .arg(to_date);
    }

    networkManager->get(QNetworkRequest(QUrl(SERV_ADDR + req_params)));
    qDebug() << SERV_ADDR + req_params;
}

void JsonProcessor::parseJsonValueToArray(QJsonValue json_array)
{
    if(json_array.isArray())
    {
        QJsonArray ja = json_array.toArray();
        for(int i = 0; i < ja.count(); i++)
        {
            Report tmpReport;
            QJsonObject subtree = ja.at(i).toObject();
            tmpReport.setId(subtree.value(REPORT_ID).toInt());
            tmpReport.setDate(subtree.value(REPORT_DATE).toInt());
            tmpReport.setRecipeTitle(subtree.value(REPORT_RECIPETITLE).toString());
            tmpReport.setFraction1(subtree.value(REPORT_FRACTION1).toInt());
            tmpReport.setFraction2(subtree.value(REPORT_FRACTION2).toInt());
            tmpReport.setFraction3(subtree.value(REPORT_FRACTION3).toInt());
            tmpReport.setFraction4(subtree.value(REPORT_FRACTION4).toInt());
            tmpReport.setFraction5(subtree.value(REPORT_FRACTION5).toInt());
            tmpReport.setFraction6(subtree.value(REPORT_FRACTION6).toInt());
            tmpReport.setBitumen(subtree.value(REPORT_BITUMEN).toDouble());
            tmpReport.setMp(subtree.value(REPORT_MP).toDouble());
            tmpReport.setCellulose1(subtree.value(REPORT_CELLULOSE1).toDouble());
            tmpReport.setCellulose2(subtree.value(REPORT_CELLULOSE2).toDouble());
            tmpReport.setDust(subtree.value(REPORT_DUST).toInt());
            tmpReport.setMixingTime(subtree.value(REPORT_MIXINGTIME).toInt());
            tmpReport.setTemperature(subtree.value(REPORT_TEMPERATURE).toInt());
            tmpReport.setTemperatureBitumen(subtree.value(REPORT_TEMPERATUREBITUMEN).toInt());
            tmpReport.setAsphalt(subtree.value(REPORT_ASPHALT).toInt());
            GlobalVariables::reports.push_back(tmpReport);
        }
        qDebug() << ja.count();
    }
}

void JsonProcessor::onResult(QNetworkReply *reply)
{
    if(!reply->error())
    {
        QJsonDocument document;
        QJsonObject root;
        QJsonValue jv;
        if(DialogSelect::global_stat == false)
        {
            document = QJsonDocument::fromJson(reply->readAll());
            root = document.object();
            qDebug() << root;
            jv = root.value("content");
            int max_pages = root.value("totalPages").toInt();

            for(int i = 1; i < max_pages + 1; i++)
            {
                GlobalVariables::pages_count.push_back(i);
            }
        }

        else
        {
            document = QJsonDocument::fromJson("{\"content\" : " + reply->readAll() + "}");
            root = document.object();
            qDebug() << root;
            jv = root.value("content");
        }

        parseJsonValueToArray(jv);

        emit finished();
    }
    else
    {
        qDebug() << "Reply error";
        emit finished();
    }
}
